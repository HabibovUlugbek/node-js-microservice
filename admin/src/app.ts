import * as express from "express";
import { Request, Response } from "express";
import * as cors from "cors";
import { createConnection } from "typeorm";
import { Product } from "./entity/product";
import * as amqp from "amqplib/callback_api";

createConnection().then((db) => {
  const productRepository = db.getRepository(Product);

  amqp.connect(
    "amqps://vtowpfwq:D3D3Gy7X3xzMiRxu4ZvVgISYXbzALi52@armadillo.rmq.cloudamqp.com/vtowpfwq",
    (error0, connection) => {
      if (error0) {
        throw error0;
      }

      connection.createChannel((errro1, channel) => {
        if (errro1) {
          throw errro1;
        }

        const app = express();

        app.use(
          cors({
            origin: ["http://localhost:3000"],
          })
        );
        app.use(express.json());

        app.get("/api/products", async (req: Request, res: Response) => {
          const products = await productRepository.find();
          res.status(200).json(products);
        });

        app.post("/api/products", async (req: Request, res: Response) => {
          const product = await productRepository.create(req.body);
          const result = await productRepository.save(product);
          channel.sendToQueue(
            "product_created",
            Buffer.from(JSON.stringify(result))
          );
          return res.send(result);
        });

        app.get("/api/products/:id", async (req: Request, res: Response) => {
          const product = await productRepository.findOne(req.params.id);
          return res.send(product);
        });

        app.put("/api/products/:id", async (req: Request, res: Response) => {
          const product = await productRepository.findOne(req.params.id);
          productRepository.merge(product, req.body);
          const result = await productRepository.save(product);
          channel.sendToQueue(
            "product_updated",
            Buffer.from(JSON.stringify(result))
          );
          return res.send(result);
        });

        app.delete("/api/products/:id", async (req: Request, res: Response) => {
          const result = await productRepository.delete(req.params.id);
          channel.sendToQueue(
            "product_deleted",
            Buffer.from(JSON.stringify(req.params.id))
          );
          return res.send(result);
        });

        app.post(
          "/api/products/:id/like",
          async (req: Request, res: Response) => {
            const product = await productRepository.findOne(req.params.id);
            product.likes++;
            const result = await productRepository.save(product);
            return res.send(result);
          }
        );

        console.log("Listening port 8000");

        app.listen(8000);

        process.on("beforeExit", () => {
          console.log("closing");
          connection.close();
        });
      });
    }
  );
});
